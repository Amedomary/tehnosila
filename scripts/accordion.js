/* ---------- Accordion  ---------- */

$(document).ready(function() {
    $('.b-js-footer__title').on('click', function(e) { //клик для открывашки
        e.preventDefault();

        var $this = $(this),
            item = $this.closest('.b-footer__column'), // элемент главного списка li = акардион
            list = $this.closest('.b-container'), //самый главный список ul
            items = list.find('.b-footer__column'), // элементы главного списка li
            content = item.find('.b-footer__link-list'), //внутрений список ul
            otherContent = list.find('.b-footer__link-list'), //внутрений список ul
            duration = 300;

        if ($(document).width() < 960) {
            if (!item.hasClass('b-js-active')) {
                items.removeClass('b-js-active');
                item.addClass('b-js-active');
                otherContent.stop(true, true).slideUp(duration);
                content.stop(true, true).slideDown(duration);
            } else {
                content.stop(true, true).slideUp(duration);
                item.stop(true, true).removeClass('b-js-active');
            }
        }
    });
});
