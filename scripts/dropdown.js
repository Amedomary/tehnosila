$(document).ready(function () {
    //выпадашка сабскрайба
    $('.b-subscribe__title').click(function (event) {
        var title = $('.b-subscribe__title'),
            duration = 300;

        if ($(document).width() < 960) {
            if (!title.hasClass('b-js-active')) {
                title.addClass('b-js-active');
            } else {
                title.removeClass('b-js-active');
            }

            $('.b-subscribe__dropdown').stop(true, true).slideToggle(duration)
        }
    });
    //Кнопка меню
    //Добавляет удаляет класс - выпадашка меню
    $(".b-menu-mobile").click(function (e) {
        e.preventDefault();
        var button = $('.b-menu-mobile'),
            nav = $('.b-mobile-nav'),
            body = $('.b-body'),
            menu = $('.b-mobile-nav');

        if (!button.hasClass('js-header-menu-open')) {
            body.addClass('js-hidden'),
            button.addClass('js-header-menu-open'),  //кнопка меню
            menu.addClass('js-mobile-nav-open');  //выезжашка мобилки
        } else {
            body.removeClass('js-hidden'),
            button.removeClass('js-header-menu-open'),
            menu.removeClass('js-mobile-nav-open');
        }
    })
});


