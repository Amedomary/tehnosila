$(document).ready(function() {
    $('.slick-brand').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        appendArrows: '.b-brand__buttons',
        responsive: [{
                    breakpoint: 750,
                    settings: {
                        slidesToShow: 3,
                    }
                },

            ] // класс в который перемещ стрелки
    });
});

// брейкпоинты поставить
$(document).ready(function() {
    $('.slick-recommend').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        appendArrows: '.b-recommend__buttons',
        responsive: [{
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                    }
                },

            ] // класс в который перемещ стрелки
    });
});

$(document).ready(function() {
    $('.slick-hw').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        appendArrows: '.b-history-watch__buttons',
        responsive: [{
                breakpoint: 960,
                settings: {
                    slidesToShow: 3,
                }
            }, {
                breakpoint: 450,
                settings: {
                    slidesToShow: 2,
                }
            }, {
                breakpoint: 350,
                settings: {
                    slidesToShow: 1,
                }
            }

        ]
    });
});
