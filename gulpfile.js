var gulp = require('gulp');
var less = require('gulp-less');
var pug = require('gulp-pug');
var concat = require('gulp-concat-css');
var browserSync = require('browser-sync');
var path = require('path');

// задача для компиляции less в css
gulp.task('less', function() {
    return gulp.src('./styles/less/main.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'bower_components') ]
        }))
        .pipe(gulp.dest('./styles/css'))
});

//компиляция pug
gulp.task('pug', function buildHTML() {
  return gulp.src('./*.pug')
  .pipe(pug({   
        pretty: '\t'
     }))
  .pipe( gulp.dest("./"))
});


// задача для вывода результата компиляции в браузере
gulp.task('browser-sync', ['less'], function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

// задача для вывода результата компиляции в браузере
gulp.task('browser-sync', ['pug'], function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

// задача для отслеживания изменений в файлах
gulp.task('watch', function () {
    gulp.watch('./styles/less/*.less', ['less']);   // cлежка для компиляции
    gulp.watch('./*.pug', ['pug']);   //слежка за pug
    gulp.watch('./styles/css/*.css').on("change", browserSync.reload);
    gulp.watch('./*.html').on('change', browserSync.reload);
    
});

// задача по умолчанию
gulp.task('default', ['browser-sync', 'watch']);